'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');

var decibelIntro =
'\n   ____      ' + "     _              _  _            _ ®" +
'\n  (**__) __  ' + "    | |            (_)| |          | |    " +
'\n /  /   /  ) ' + "  __| |  ___   ___  _ | |__    ___ | |    " +
'\n(__/ __/  /  ' + " / _` | / _ \\ / __|| || '_ \\  / _ \\| | " +
'\n    (____)   ' + "| (_| ||  __/| (__ | || |_) ||  __/| |    " +
'\n             ' + "'\\__,_| \\___| \\___||_||_.__/  \\___||_|" +
'\n                                                                 '+
'\nWelcome to the Decibel Front-end App Builder.' +
'\nJust follow the prompts to start your project.'+
'\n'+ 
'\nOnce completed, simply run ' + '\'grunt server\'' + ' to start working'+
'\n';

var DecibelGenerator = module.exports = function DecibelGenerator(args, options, config) {

  yeoman.generators.Base.apply(this, arguments);

  this.on('end', function () {
    this.installDependencies({ skipInstall: options['skip-install'] });
  });

  this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(DecibelGenerator, yeoman.generators.Base);

DecibelGenerator.prototype.askFor = function askFor() {
  var cb = this.async();

  // have Yeoman greet the user.
  // console.log(this.yeoman);

  console.log(decibelIntro);

  var prompts = [
    {
      // type: 'confirm',
      name: 'appName',
      message: 'What is the name of the App?',
    },
    {
      type: 'checkbox',
      name: 'features',
      message: 'What features would you like?',
      choices: [
        {
          name: 'Bootstrap 3 (Less)',
          value: 'lessBootstrap',
          checked: true
        },
        {
          name: 'Enquire JS',
          value: 'includeEnquire',
          checked: true
        }
      ]
    }
  ];

  this.prompt(prompts, function (props) {
    var features = props.features;

    this.appName = props.appName;
    this.lessBootstrap = features.indexOf('lessBootstrap') !== -1
    this.includeEnquire = features.indexOf('includeEnquire') !== -1

    cb();
  }.bind(this));
};

DecibelGenerator.prototype.app = function app() {
  this.mkdir('app');
  this.mkdir('app/fonts');
  this.mkdir('app/less');
  this.mkdir('app/less/extend');
  this.mkdir('app/images');
  this.mkdir('app/partials');
  this.mkdir('app/scripts');

  // html
  this.template('index.html', 'app/index.html');
  this.template('partials/_header.html', 'app/partials/header.html');
  this.template('partials/footer.html', 'app/partials/footer.html');

  // js 
  this.template('scripts/_template.js', 'app/scripts/template.js');
  this.template('scripts/enquire.js', 'app/scripts/enquire.js');
  this.template('scripts/jquery.js', 'app/scripts/jquery.js');
  
  // css
  this.template('styles/extend/variables.less', 'app/styles/extend/variables.less');  
  this.template('styles/style_base.less', 'app/styles/style_base.less');
  this.template('styles/style_all.less', 'app/styles/style_all.less');
  this.template('styles/style_screen.less', 'app/styles/style_screen.less');
  this.template('styles/style_responsive.less', 'app/styles/style_responsive.less');
  this.template('styles/style_icons.less', 'app/styles/style_icons.less');

  // build files
  this.template('_Gruntfile.js', 'Gruntfile.js');
  this.template('_bower.json', 'bower.json');
  this.template('_config.json', 'config.json');
  this.template('_package.json', 'package.json');
  
};

DecibelGenerator.prototype.runtime = function runtime() {
  this.copy('bowerrc', '.bowerrc');
  // this.copy('gitignore', '.gitignore');
};

DecibelGenerator.prototype.projectfiles = function projectfiles() {
  this.copy('editorconfig', '.editorconfig');
  this.copy('jshintrc', '.jshintrc');
};
