/**
 * <%= _.capitalize(appName) %> - Template Javascript
 *
 * Try and keep as much styling in the CSS Breakpoints and only use the 
 * JS to fire and bind events at breakpoints. Examples below.
 *  
 * Repsonsive JS made possible using enquire.js
 * http://wicky.nillia.ms/enquire.js/
 * https://github.com/WickyNilliams/enquire.js
 * 
 */

(function($){

    // Setup namespace
    $.<%= _.capitalize(appName) %> = {};

    // Namespace for enquire.js Match and Unmatch.
    $.<%= _.capitalize(appName) %>.Match = {};
    $.<%= _.capitalize(appName) %>.Unmatch = {};

    // Add Breakpoints to namespace
    $.<%= _.capitalize(appName) %>.breakPoints = {
        smallScreen  : "(max-width:767px)",
        mediumScreen : "(min-width: 768px) and (max-width: 979px)",
        largeScreen  : "(min-width: 980px)",
        xLargeScreen : "(min-width: 1200px)"
    };

    /**
     * <%= _.capitalize(appName) %> - Small Screen Match 
     */
    $.<%= _.capitalize(appName) %>.Match.smallScreen = function() {

    };

    /**
     * <%= _.capitalize(appName) %> - Small Screen Unmatch  
     */
    $.<%= _.capitalize(appName) %>.Unmatch.smallScreen = function() {

    };

    /**
     * <%= _.capitalize(appName) %> - Medium Screen Match  
     */
    $.<%= _.capitalize(appName) %>.Match.mediumScreen = function() {

    };

    /**
     * <%= _.capitalize(appName) %> - Medium Screen Unmatch  
     */
    $.<%= _.capitalize(appName) %>.Unmatch.mediumScreen = function() {

    };

    /**
     * <%= _.capitalize(appName) %> - Large Screen Match  
     */
    $.<%= _.capitalize(appName) %>.Match.largeScreen = function() {

    };

    /**
     * <%= _.capitalize(appName) %> - Large Screen Unmatch  
     */
    $.<%= _.capitalize(appName) %>.Unmatch.largeScreen = function() {

    };

    /**
     * <%= _.capitalize(appName) %> - Extra Screen Match  
     */
    $.<%= _.capitalize(appName) %>.Match.xLargeScreen = function() {

    };

    /**
     * <%= _.capitalize(appName) %> - Extra Large Screen Unmatch  
     */
    $.<%= _.capitalize(appName) %>.Unmatch.xLargeScreen = function() {

    };

    /** Call Enquire to check breakpoints and fire functions**/

    enquire.register(
        $.<%= _.capitalize(appName) %>.breakPoints.smallScreen,
        {
            match : function() {$.<%= _.capitalize(appName) %>.Match.smallScreen();},
            unmatch: function() {$.<%= _.capitalize(appName) %>.Unmatch.smallScreen();}
        }
    ).register(
        $.<%= _.capitalize(appName) %>.breakPoints.mediumScreen,
        {
            match : function() {$.<%= _.capitalize(appName) %>.Match.mediumScreen();},
            unmatch: function() {$.<%= _.capitalize(appName) %>.Unmatch.mediumScreen();}
        }
    ).register(
        $.<%= _.capitalize(appName) %>.breakPoints.largeScreen,
        {
            match : function() {$.<%= _.capitalize(appName) %>.Match.largeScreen();},
            unmatch: function() {$.<%= _.capitalize(appName) %>.Unmatch.largeScreen();}
        }
    ).register(
        $.<%= _.capitalize(appName) %>.breakPoints.xLargeScreen,
        {
            match : function() {$.<%= _.capitalize(appName) %>.Match.xLargeScreen();},
            unmatch: function() {$.<%= _.capitalize(appName) %>.Unmatch.xLargeScreen();}
        }
    );

})(jQuery);

/** Document Ready Functions **/
/********************************************************************/

$(document).ready(function() {

    console.log('Hello World, this is <%= _.capitalize(appName) %>')

});

/** Reusable Functions **/
/********************************************************************/