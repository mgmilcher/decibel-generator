// Generated on 2013-10-16 using generator-decibel 0.0.4
'use strict';
var moment = require('moment');

var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};

module.exports = function (grunt) {
  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    watch: {
      options: {
        nospawn: true,
        livereload: LIVERELOAD_PORT
      },
      livereload: {
        files: [
        'app/**/*'
        ],
        tasks: ['build']
      }
    },
    connect: {
      options: {
        port: 9000,
        // change this to 'localhost' to disallow access to the server 
        // from outside
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
            lrSnippet,
            mountFolder(connect, './build/')
            ];
          }
        }
      }
    },
    open: {
      server: {
        path: 'http://localhost:<%= connect.options.port %>'
      }
    },
    bake: {
      build: {
        options: {
        // Task-specific options go here.
      },
      files: [{
          // Enable dynamic expansion.
          expand: true,     
          // Src matches are relative to this path.
          cwd: 'app/',
          // Actual pattern(s) to match.
          src: ['!partials/**', '*.html'],
          // Destination path prefix.
          dest: 'build/',
          // Dest filepaths will have this extension.
          ext: '.html',
        }]
      },
    },
    copy: {
      build: {
        files: [
          {expand: true, flatten: true, src: ['app/fonts/**'], dest: 'build/fonts/', filter: 'isFile'},
          {expand: true, flatten: true, src: ['app/images/**'], dest: 'build/images/', filter: 'isFile'},
          {expand: true, flatten: true, src: ['app/scripts/**'], dest: 'build/scripts/', filter: 'isFile'},
          {expand: true, flatten: true, src: ['app/*.md'], dest: 'build/', filter: 'isFile'},
          // {expand: true, flatten: true, src: ['bower_components/jquery/jquery.js'], dest: 'build/scripts/', filter: 'isFile'},
          // {expand: true, flatten: true, src: ['bower_components/enquire/enquire.js'], dest: 'build/scripts/', filter: 'isFile'}
        ]
      }
    },
    clean: {
      build: {
        files: [{
          dot: true,
          src: [
            'build/'
          ]
        }]
      },
    },
    less: {
      build: {
        options: {
          paths: ["app/styles"],
          optimization: 0
        },
        files: [{
          // Enable dynamic expansion.
          expand: true,     
          // Src matches are relative to this path.
          cwd: 'app/styles',
          // Actual pattern(s) to match.
          src: ['**/*.less', '!**/extend/**'],
          // Destination path prefix.
          dest: 'build/styles',
          // Dest filepaths will have this extension.
          ext: '.css',
        }]
      }
    }
  });

  grunt.registerTask('server', [
    'build',
    'connect:livereload',
    'open',
    'watch'
  ]);

  grunt.registerTask('build', [
    'default'
  ]);

  grunt.registerTask('default', [
    'clean:build',
    'less:build',
    'bake:build',
    'copy:build'
  ]);

};