# Generator-decibel
[![Build Status](https://secure.travis-ci.org/mgmilcher/generator-decibel.png?branch=master)](https://travis-ci.org/mgmilcher/generator-decibel)

A generator for Yeoman.

## Requirements
- [Node.js >= 0.8.0](http://nodejs.org/)
- [Grunt ~0.4.0](http://gruntjs.com/getting-started)
- [GIT CLI](http://git-scm.com/downloads)

## Getting started
- Make sure you have [yo](https://github.com/yeoman/yo) installed:
    `npm install -g yo`
- Install the generator: `npm install -g https://bitbucket.org/mgmilcher/decibel-generator/get/master.tar.gz`
- Run: `yo decibel`

## License
[MIT License](http://en.wikipedia.org/wiki/MIT_License)
